# ************************************************************
# Sequel Pro SQL dump
# Vers�o 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Base de Dados: gestaoab
# Tempo de Gera��o: 2018-11-01 08:04:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela adendos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `adendos`;

CREATE TABLE `adendos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `descricao resumida` text,
  `descricao completa` longtext,
  `projeto` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `data inicio` date DEFAULT NULL,
  `prazo` int(11) DEFAULT NULL,
  `data previsao entrega` date DEFAULT NULL,
  `valor` decimal(5,2) DEFAULT NULL,
  `data pagamento` date DEFAULT NULL,
  `parcelas` int(11) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `adendos` WRITE;
/*!40000 ALTER TABLE `adendos` DISABLE KEYS */;

INSERT INTO `adendos` (`id`, `nome`, `descricao resumida`, `descricao completa`, `projeto`, `status`, `data inicio`, `prazo`, `data previsao entrega`, `valor`, `data pagamento`, `parcelas`, `ts`)
VALUES
	(1,'teste06','teste de POST',NULL,'teste de postman',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-31 11:45:50');

/*!40000 ALTER TABLE `adendos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `foto` blob,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;

INSERT INTO `clientes` (`id`, `nome`, `foto`, `ts`)
VALUES
	(0,'teste',X'746573746574616D62656D','2018-10-29 20:51:01'),
	(1,'teste05',X'746573746520646520706F73746D616E','2018-10-31 11:34:35');

/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela contatos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contatos`;

CREATE TABLE `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefone` bigint(11) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contatos` WRITE;
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;

INSERT INTO `contatos` (`id`, `nome`, `email`, `telefone`, `foto`, `idCliente`, `ts`)
VALUES
	(1,'teste01',NULL,NULL,NULL,NULL,'2018-10-31 10:44:02');

/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela cortesias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cortesias`;

CREATE TABLE `cortesias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `descricao tecnica` text,
  `descricao cliente` text,
  `descricao contratual` text,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cortesias` WRITE;
/*!40000 ALTER TABLE `cortesias` DISABLE KEYS */;

INSERT INTO `cortesias` (`id`, `nome`, `descricao tecnica`, `descricao cliente`, `descricao contratual`, `ts`)
VALUES
	(1,NULL,NULL,NULL,NULL,'2018-10-30 21:50:20'),
	(2,'teste01',NULL,NULL,NULL,'2018-10-30 21:51:34');

/*!40000 ALTER TABLE `cortesias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela funcionalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `funcionalidades`;

CREATE TABLE `funcionalidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `valor` decimal(4,2) DEFAULT NULL,
  `descricao tecnica` text,
  `descricao cliente` text,
  `descricao contratual` text,
  `horas desenvolvimento` text,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `funcionalidades` WRITE;
/*!40000 ALTER TABLE `funcionalidades` DISABLE KEYS */;

INSERT INTO `funcionalidades` (`id`, `nome`, `valor`, `descricao tecnica`, `descricao cliente`, `descricao contratual`, `horas desenvolvimento`, `ts`)
VALUES
	(1,'teste01',NULL,NULL,NULL,NULL,NULL,'2018-10-30 21:52:28');

/*!40000 ALTER TABLE `funcionalidades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela orcamentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orcamentos`;

CREATE TABLE `orcamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projeto` varchar(100) DEFAULT NULL,
  `descricao` text,
  `valor` decimal(5,2) DEFAULT NULL,
  `prazo` int(11) DEFAULT NULL,
  `obsvervacoes` text,
  `metodo de realizacao projeto` text,
  `comissao` decimal(3,2) DEFAULT NULL,
  `valor final` decimal(5,2) DEFAULT NULL,
  `validade` int(11) DEFAULT NULL,
  `aprovar orcamento` tinyint(4) DEFAULT NULL,
  `desconto` decimal(2,0) DEFAULT NULL,
  `observacao tecnica` text,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `orcamentos` WRITE;
/*!40000 ALTER TABLE `orcamentos` DISABLE KEYS */;

INSERT INTO `orcamentos` (`id`, `projeto`, `descricao`, `valor`, `prazo`, `obsvervacoes`, `metodo de realizacao projeto`, `comissao`, `valor final`, `validade`, `aprovar orcamento`, `desconto`, `observacao tecnica`, `ts`)
VALUES
	(1,'teste01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-31 10:54:10'),
	(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-31 10:56:12'),
	(3,NULL,'teste de postman',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-31 10:57:31'),
	(4,'teste03','teste de postman',999.99,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-31 11:07:45');

/*!40000 ALTER TABLE `orcamentos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela projetos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projetos`;

CREATE TABLE `projetos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descricao resumida` text,
  `descricao completa` longtext,
  `data primeiro contato` date DEFAULT NULL,
  `nome projeto cliente` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `data inicio` date DEFAULT NULL,
  `prazo` int(11) DEFAULT NULL,
  `valor` decimal(5,2) DEFAULT NULL,
  `data pagamento` date DEFAULT NULL,
  `parcelas` int(11) DEFAULT NULL,
  `encerrado` varchar(45) DEFAULT NULL,
  `cliente` varchar(45) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;

INSERT INTO `projetos` (`id`, `nome`, `descricao resumida`, `descricao completa`, `data primeiro contato`, `nome projeto cliente`, `status`, `data inicio`, `prazo`, `valor`, `data pagamento`, `parcelas`, `encerrado`, `cliente`, `ts`)
VALUES
	(0,'teste04','teste de postman',NULL,NULL,NULL,NULL,NULL,NULL,999.99,NULL,NULL,NULL,NULL,'2018-10-31 11:21:36');

/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
