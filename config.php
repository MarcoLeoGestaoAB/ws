<?php
require_once("./vendor/autoload.php");
use Medoo\Medoo;

define('DIR_BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class) {
    if (file_exists(DIR_BASE . '/models/' . $class . '.php')) {
        require_once 'models/' . $class . '.php';
    }
    if (file_exists(DIR_BASE . '/controllers/' . $class . '.php')) {
        require_once 'controllers/' . $class . '.php';
    }
});
 
$_BD = new Medoo([
	// required
	'database_type' => 'mysql',
	'database_name' => 'gestaoab',
	'server' => 'localhost',
	'username' => 'root',
	'password' => '',
 

	// [optional] Medoo will execute those commands after connected to the database for initialization
	'command' => [
		"SET lc_time_names = 'pt_BR'"
	]
]);
