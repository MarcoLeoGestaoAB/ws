<?php
require_once("config.php");
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
$app->get('/teste', function (Request $request, Response $response, array $args) {
    $name = "TesteAPI";
    $response->getBody()->write("Hello, $name");

    return $response;
});

foreach (glob("rotas/*") as $filename) {
    require_once $filename;
}

$app->run();