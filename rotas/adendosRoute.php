<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app->group('/adendos', function(){
    $this->get('/', function (Request $request, Response $response, array $args) {
        $parsedBody = $request->getQueryParams();
        print_r($parsedBody);
        print_r($args);
        exit;
    
        return $response;
    });

    $this->post('/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $AdendoCtrl = new AdendoCtrl();
        $AdendoCtrl->insert($params);
        exit;
    
        return $response;
    });

    $this->post('/delete/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $AdendoCtrl = new AdendoCtrl();
        $AdendoCtrl->delete($params);
        exit;
    
        return $response;
    });

    $this->post('/update/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $AdendoCtrl = new AdendoCtrl();
        $AdendoCtrl->update($params);
        exit;
    
        return $response;
    });
});