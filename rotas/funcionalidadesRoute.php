<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app->group('/funcionalidades', function(){
    $this->get('/', function (Request $request, Response $response, array $args) {
        $parsedBody = $request->getQueryParams();
        print_r($parsedBody);
        print_r($args);
        exit;
    
        return $response;
    });

    $this->post('/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $FuncionalidadeCtrl = new FuncionalidadeCtrl();
        $FuncionalidadeCtrl->insert($params);
        exit;
    
        return $response;
    });

    $this->post('/delete/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $FuncionalidadeCtrl = new FuncionalidadeCtrl();
        $FuncionalidadeCtrl->delete($params);
        exit;
    
        return $response;
    });

    $this->post('/update/', function (Request $request, Response $response, array $args) {
        $params = $request->getParsedBody();
        $FuncionalidadeCtrl = new FuncionalidadeCtrl();
        $FuncionalidadeCtrl->update($params);
        exit;
    
        return $response;
    });
});