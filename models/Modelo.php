<?php
class Modelo extends Auxiliar
{
    protected $table = "";
    protected $view = "";
    protected $_BD;
    protected $mostraQuery;

    public function __construct($mostraQuery = false){
        global $_BD;
        $this->mostraQuery = $mostraQuery;
        $this->_BD = $_BD;
    }

    public function insert($crud){
        return $this->_BD->insert($this->table, $crud);        
    }

    public function update($crud){
        return $this->_BD->update($this->table, $crud, ["id" => 2]);        
    }

    public function delete($crud){
        return $this->_BD->delete($this->table, $crud);        
    }
}

